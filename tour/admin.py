from django.contrib import admin
from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from .models import Tour

class TourAdminForm(forms.ModelForm):
    """Форма с виджетом ckeditor"""
    description = forms.CharField(label="Описание", widget=CKEditorUploadingWidget())
    text = forms.CharField(label="Текст", widget=CKEditorUploadingWidget())
    class Meta:
        model = Tour
        fields = '__all__'

@admin.register(Tour)
class TourAdmin(admin.ModelAdmin):
    """Тури"""
    list_display = ("title", "slug", "price")
    list_display_links = ("title",)
    form = TourAdminForm