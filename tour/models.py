from django.db import models

class Tour(models.Model):
    """Категории"""
    title = models.CharField("Заголовок", max_length=255)
    slug = models.SlugField(max_length=255, unique=True)
    price = models.PositiveIntegerField("Ціна")
    description = models.TextField("Описание")
    text = models.TextField("Текст")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Тур"
        verbose_name_plural = "Тури"