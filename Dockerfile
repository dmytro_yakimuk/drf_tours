FROM python:3-slim
ENV PYTHONUNBUFFERED 1

WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt && chmod +x entrypoint.sh
#
#RUN apt-get update && apt-get install netcat -y
#RUN apt-get upgrade -y && apt-get install postgresql gcc python3-dev musl-dev -y
#RUN pip install --upgrade pip
#
#CMD sh entrypoint.sh
